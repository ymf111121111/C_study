#include "CustomString.h"



CustomString::CustomString()
{
}

CustomString::CustomString(const char* str)
{

	if (nullptr == str)
	{
		return;
	}
	this->strlength = strlen(str);
	customcpy(str);
}

CustomString::CustomString(const CustomString& other)
{
	delete[] _str;
	this->strlength = strlen(other._str);
	customcpy(other._str);

}

CustomString CustomString::sub(int start, int lens)
{
	if (start < 0 || start > this->strlength || lens < 0 || lens > strlength || start + lens > strlength)
	{
		return *this;
	}

	CustomString tempstr;
	tempstr.strlength = lens;
	tempstr._str = new char[lens + 1];
	for (int i = 0; i < lens; ++i)
	{
		tempstr._str[i] = this->_str[i + start];
	}
	tempstr._str[lens] = '\0';
	return tempstr;
}

std::vector<CustomString> CustomString::split(const char div)
{
	std::vector<CustomString> res;
	std::vector<int> divindex = finddiv(div);

	int start = 0, lens = 0;
	for (int i = 0; i < divindex.size(); i++)
	{
		lens = divindex[i] - start - 1;
		res.push_back(sub(start, lens));
		start = divindex[i];
	}
	lens = strlength - start;
	res.push_back(sub(start, lens));
	return res;
}

CustomString& CustomString::operator=(const CustomString& other)
{
	this->strlength = strlen(other._str);
	delete[] _str;
	customcpy(other._str);
	return *this;
}

void CustomString::append(const char* str)
{
	int lens = strlen(str);
	int templens = strlength;
	strlength += lens;
	char* tempstr = _str;
	_str = new char[strlength + 1];
	customcpy(tempstr);
	for (int i = 0; i < lens; ++i)
	{
		_str[i + templens] = str[i];
	}
	_str[strlength] = '\0';

	delete[] tempstr;


}

CustomString::~CustomString()
{
	delete[] _str;
}

int CustomString::strlen(const char* str)
{
	int lens = 0;
	while (1)
	{
		if (str[lens] == '\0' || str == nullptr) break;
		lens++;
	}
	return lens;
}

void CustomString::customcpy(const char* other)
{
	this->_str = new char[this->strlength + 1];
	for (int i = 0; i < this->strlength; ++i)
	{
		this->_str[i] = other[i];
	}
	this->_str[this->strlength] = '\0';
}

int CustomString::len()
{
	return this->strlength;
}



int CustomString::find(const char* targetstr)
{
	int m = strlength, n = strlen(targetstr);
	int* ne = new int[m + 1];
	memset(ne, 0, (m + 1) * sizeof(ne));
	//模式串
	char* s_str = new char[strlength + 2];
	//模板串
	char* p_str = new char[n + 2];
	//为了好匹配 将两个string右平移一位 
	for (int i = 0; i < strlength; i++)
	{
		s_str[i + 1] = _str[i];
	}

	for (int i = 0; i < n; i++)
	{
		p_str[i + 1] = targetstr[i];
	}

	for (int i = 2, j = 0; i <= n; i++)
	{
		while (j && p_str[j + 1] != p_str[i]) j = ne[j];
		if (p_str[j + 1] == p_str[i]) j++;
		ne[i] = j;
	}

	for (int i = 1, j = 0; i <= m; i++)
	{
		while (j && p_str[j + 1] != s_str[i]) j = ne[j];
		if (p_str[j + 1] == s_str[i]) j++;
		if (j == n)
		{
			delete[] s_str;
			delete[] p_str;
			j = ne[j];
			return i - j;
		}
	}
	return -1;
}

std::vector<int> CustomString::finddiv(const char div)
{
	int m = strlength, n = 1;
	int* ne = new int[m + 1];
	memset(ne, 0, (m + 1) * sizeof(ne));
	//模式串
	char* s_str = new char[strlength + 2];
	//模板串
	char* p_str = new char[n + 2];
	//为了好匹配 将两个string右平移一位 
	for (int i = 0; i < strlength; i++)
	{
		s_str[i + 1] = _str[i];
	}
	p_str[1] = div;

	for (int i = 2, j = 0; i <= n; i++)
	{
		while (j && p_str[j + 1] != p_str[i]) j = ne[j];
		if (p_str[j + 1] == p_str[i]) j++;
		ne[i] = j;
	}

	std::vector<int> res;

	for (int i = 1, j = 0; i <= m; i++)
	{
		while (j && p_str[j + 1] != s_str[i]) j = ne[j];
		if (p_str[j + 1] == s_str[i]) j++;
		if (j == n)
		{

			j = ne[j];
			res.push_back(i - j);
		}
	}
	delete[] s_str;
	delete[] p_str;
	return res;
}

bool CustomString::operator==(const CustomString& other)
{
	int leftstr = strlength;
	int rightstr = other.strlength;

	if (leftstr != rightstr) return false;

	for (int i = 0; i < leftstr; i++)
	{
		if (_str[i] != other._str[i]) return false;
	}

	return true;
}


