#include "CustomString.h"
#include <iostream>
#include <vector>
using namespace std;

int main()
{

	auto str1 = CustomString("test1");

	auto str2 = CustomString("test2, test3,124561,45749,444");

	vector<CustomString> ret = str2.split(',');
	for (int i = 0; i < ret.size(); i++)
	{
		cout << ret[i]._str << endl;
	}
	return 0;
}
