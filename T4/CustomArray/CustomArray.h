#pragma once
#include <iostream>
#include <memory>
using namespace std;

template<typename T>
class CustomArray
{
public:
	CustomArray();
	~CustomArray();

	void reserve( int customsize);
	void remove( int index);
	void insert( T val, int index);
	void push( T val);
	void pop();
	int findindex( T val);
	void clear();
private:
	void ExtensionSize();
	void Extension(int Size);
public :
	T* Data = nullptr;
	int _Capacity = 0;
	int _Size = 0;
};

template<typename T>
inline CustomArray<T>::CustomArray()
{
	
}

template<typename T>
inline CustomArray<T>::~CustomArray()
{
	delete []Data;
}

template<typename T>
void CustomArray<T>::reserve(int CustomSize)
{
	if (CustomSize <= _Size) return;
	Extension(CustomSize);
	_Capacity = CustomSize;
}

template<typename T>
void CustomArray<T>::remove(int index)
{
	if (index >= _Size) return;
	for (int i = index+1; i < _Size; i++)
	{
		Data[i - 1] = Data[i];
	}
	_Size--;
}

template<typename T>
void CustomArray<T>::insert(T val,int index)
{
	_Size++;
	if (index < 0 || index >= _Size) return;

	if (_Size >= _Capacity) ExtensionSize();
	for (int i = _Size - 2; i > index; i--)
	{
		Data[i+1] = Data[i];
	}
	Data[index] = val;
}

template<typename T>
void CustomArray<T>::push(T val)
{
	if (_Size >= _Capacity) ExtensionSize();
	Data[_Size++] = val;
}

template<typename T>
void CustomArray<T>::pop()
{
	_Size--;
}

template<typename T>
int CustomArray<T>::findindex(T val )
{
	for (int i = 0; i < _Size; i++)
	{
		if (Data[i] == val)return i;
	}
	return -1; 
}

template<typename T>
void CustomArray<T>::clear()
{
	_Capacity = 0;
	_Size = 0;
}

template<typename T>
inline void CustomArray<T>::ExtensionSize()
{
	int TargetCapacity = 0;
	for (int i = 2; 1; i = i * 2)
	{
		if (_Capacity < i)
		{
			TargetCapacity = i;
			break;
		}
	}
	Extension(TargetCapacity);
}

template<typename T>
void CustomArray<T>::Extension(int Size)
{
	
	_Capacity = Size;
	auto TempData = new T[_Capacity];
	for (int i = 0; i < _Size; i++)
	{
		TempData[i] = Data[i];
	}
	delete []Data;
	Data = TempData;

	
}
