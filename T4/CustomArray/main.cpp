#include <iostream>
#include <memory>

#include "CustomArray.h"

using namespace std;

/*
实现数组以及一下api
auto arr = new CustomArray();
arr.reserve(4);
arr.push(1);
arr.push(2);
arr.insert(1, 3);
arr.remove(1);
arr.pop();
arr.clear();
int index = arr.findIndex(1);

做完上面的再改成带模板的
*/
int main()
{
	auto arr = new CustomArray<int>();
	arr->reserve(4);
	arr->push(1);
	arr->push(2);
	arr->pop();
	arr->push(3);
	arr->insert(1, 3);
	cout << arr->findindex(3) << endl;
	cout << arr->Data[1] <<endl; 
	return 0;
}