#pragma once
#include <iostream>

template <typename T>
struct Node
{
	T Val;
	Node<T> *NextNode = nullptr;
	Node<T> *PreNode = nullptr;
	Node() {};
	Node(int x) : Val(x){};
};

template <typename T>
class CustomList
{
public:
	CustomList();
	~CustomList();

	void Push(T Val);
	void Insert(T Val, int Index);
	Node<T>* Find(int Index);
	void Remove(int Index);
	void Pop();
	void PopAll();

	void Foreach();

private:
	void deleteAll();

private:
	Node<T> *_HeadNode;
};

template <typename T>
inline CustomList<T>::CustomList()
{
	this->_HeadNode = new Node<T>();
}

template <typename T>
inline CustomList<T>::~CustomList()
{
	this->deleteAll();
}

template <typename T>
void CustomList<T>::Push(T Val)
{
	Node<T> *NewNode = new Node<T>(Val);
	Node<T> *CurrentNode = this->_HeadNode;
	while (CurrentNode->NextNode != nullptr)
	{
		CurrentNode = CurrentNode->NextNode;
	}

	CurrentNode->NextNode = NewNode;
	NewNode->PreNode = CurrentNode;
}


template <typename T>
void CustomList<T>::Insert(T Val, int Index)
{
	Node<T> *CurrentNode = this->_HeadNode->NextNode;
	while (CurrentNode != nullptr || Index <= 0)
	{
		CurrentNode = CurrentNode->NextNode;
		Index--;
	};
	
	Node<T> *NewNode = new Node<T>(Val);
	Node<T> *TempPreNode = CurrentNode->PreNode;

	NewNode->NextNode = CurrentNode;
	NewNode->PreNode = TempPreNode;

	TempPreNode->NextNode = NewNode;
}

template <typename T>
Node<T>* CustomList<T>::Find(int Index)
{
	Node<T> *CurrentNode = this->_HeadNode->NextNode;
	while (CurrentNode != nullptr || Index <= 0)
	{
		CurrentNode = CurrentNode->NextNode;
		Index--;
	};

	return CurrentNode;
}

template <typename T>
void CustomList<T>::Remove(int Index)
{
	Node<T> *RemoveNode = this->Find(Index);
	RemoveNode->PreNode->NextNode = RemoveNode->NextNode;

	delete RemoveNode;
}

template <typename T>
void CustomList<T>::Pop()
{
	Node<T> *CurrentNode = this->_HeadNode->NextNode;
	while (CurrentNode != nullptr)
	{
		CurrentNode = CurrentNode->NextNode;
	};

	CurrentNode->PreNode->NextNode = nullptr;
	
	delete CurrentNode;
}

template <typename T>
void CustomList<T>::PopAll()
{
	if(this->_HeadNode == nullptr)
	{
		return;
	}
	
	this->deleteAll();
}

template <typename T>
void CustomList<T>::Foreach()
{
	Node<T> *CurrentNode = this->_HeadNode->NextNode;
	while (CurrentNode != nullptr)
	{
		std::cout<< CurrentNode->Val << std::endl;
		CurrentNode = CurrentNode->NextNode;
	};
}

template <typename T>
void CustomList<T>::deleteAll()
{
	Node<T> *CurrentNode = this->_HeadNode->NextNode;
	while (CurrentNode != nullptr)
	{
		delete CurrentNode->PreNode;
		CurrentNode = CurrentNode->NextNode;
	};
	delete CurrentNode;
}
