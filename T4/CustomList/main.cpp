#include <iostream>
#include "CustomList.h"

/*
实现双向链表以下api
auto list1 = new CustomList();
auto node1 = list1.push(1);
auto node2 = list1.push(2);
auto node3 = list1.insert(node2, 3);
auto ret = list1.find(2);
list1.remove(node2);
list1.popAll();
*/
int main()
{
	std::cout << "Start Test:" << std::endl;
	auto List1 = new CustomList<int>();
	List1->Push(999999);
	List1->Push(1);
	List1->Push(2);
	List1->Foreach();
	return 0;
}