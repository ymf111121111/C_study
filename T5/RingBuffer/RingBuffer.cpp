#include "RingBuffer.h"
#include <iostream>
using namespace std;
#define min(a, b) ((a) < (b) ? (a) : (b))

RingBuffer::RingBuffer(int lens)
{
    Data = new char[lens];
    Front = 0;
    Rear = 0;
    Size = lens;
}

RingBuffer::~RingBuffer()
{
    delete Data;
}

int RingBuffer::FreeSpace()
{
    return Size - (Front - Rear);
}

bool RingBuffer::IsEmpty()
{
    return Front == Rear;
}

void RingBuffer::Push(const char *SourceData, int Lens)
{
    int FreeSpace = this->FreeSpace();
    if (Lens > FreeSpace)
    {
        cout << " space not enough." << endl;
        this->Expend(FreeSpace, Lens);
    }

    int i = min(Lens, Size - Front % Size);
    memcpy(Data + (Front % Size), SourceData, i);
    memcpy(Data, SourceData + i, Lens - i);
    Front += Lens;
    cout << "res :" << Data;
}

void RingBuffer::Pop(char *SourceData, int Lens)
{
    int i = min(Lens, Size - Rear % Size - 1);
    memcpy(SourceData, Data + Rear % Size - 1, i);
    memcpy(SourceData + i, Data, Lens - i);
    Rear += Lens;
}

void RingBuffer::Expend(int FreeSpace, int Lens)
{
    while (1)
    {
        FreeSpace *= 2;
        if (FreeSpace >= Lens)
            break;
    }
    char *NewData = new char(FreeSpace);
    memcpy(NewData, Data, strlen(Data));
    delete Data;

    char *Data = NewData;
    Size = FreeSpace;
}

void RingBuffer::Lessen()
{
}