#pragma once

class RingBuffer
{
public:
    RingBuffer(int lens);
    ~RingBuffer();
    void Push(const char *SourceData, int Lens);
    void Pop(char *SourceData, int Lens);
    void Expend(int FreeSpace, int Lens);
    void Lessen();
    int FreeSpace();
    bool IsEmpty();

private:
    unsigned int Size = 0;
    int Front;
    int Rear;
    char *Data;
};