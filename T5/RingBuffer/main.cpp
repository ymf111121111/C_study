#include <iostream>
#include <cstring>
#include "RingBuffer.h"

using namespace std;

int main()
{
    auto r = new RingBuffer(4);
    const char *testData = "1234567";
    const int lens = strlen(testData) + 1;

    r->Push(testData, lens);
    char res[128];
    r->Pop(res, lens);
    return 0;
}