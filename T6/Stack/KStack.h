#include <iostream>
#include <vector>

class KStack
{
public:
    template <typename T>
    void Push(T &Value);
    template <typename T>
    void Pop(T &Value);

private:
    std::vector<void *> Data;
    int CurrentIndex = -1;
};

template <typename T>
void KStack::Push(T &Value)
{
    Data.push_back(&Value);

    CurrentIndex++;
    // value1 = *(T*)_data[_index];
}

template <typename T>
void KStack::Pop(T &Value)
{
    Value = *(T *)Data[CurrentIndex];
    // value = (T)(*_data[_index]);
    CurrentIndex--;
}