#include <iostream>
#include "KStack.h"

int main()
{
    KStack *TestStack = new KStack();
    int a = 20;
    float b = 11.2;
    float resB = 1.0;
    int resA = 0;

    TestStack->Push(a);
    TestStack->Push(b);

    TestStack->Pop(resB);
    TestStack->Pop(resA);
    std::cout << "resB : " << resB << std::endl;
    std::cout << "resA : " << resA << std::endl;
    return 0;
}