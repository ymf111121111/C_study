#pragma once

#include<iostream>

class Point;
class Octree;

enum class EQuadrant {
	BOTTOM_RIGHT_BACK = 0, // 下 右 后 为最小
	BOTTOM_RIGHT_FRONT,
	BOTTOM_LEFT_BACK,
	BOTTOM_LEFT_FRONT,
	TOP_RIGHT_BACK,
	TOP_RIGHT_FRONT,
	TOP_LEFT_BACK,
	TOP_LEFT_FRONT,		   // 上 左 前 为最大
};

using PointPtr = std::shared_ptr<Point>;
using OctreePtr = std::shared_ptr<Octree>;

