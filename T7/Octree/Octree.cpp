#pragma once
#include "Octree.h"

// 空立方体
Octree::Octree()
{
    this->RootPoint = std::make_shared<Point>();
}

// 包含元素的立方体
Octree::Octree(int x, int y, int z)
{
    this->RootPoint = std::make_shared<Point>(x, y, z);
}

// 创建立方体
Octree::Octree(int xMax, int xMin, int yMax, int yMin, int zMax, int zMin)
{
    if (xMax < xMin || yMax < yMin || zMax < zMin) {
        std::cout << "There is no boundary" << std::endl;
    }

    this->MaxRange = std::make_shared<Point>(xMax, yMax, zMax);
    this->MinRange = std::make_shared<Point>(xMin, yMin, zMin);

    for (int i = (int)EQuadrant::BOTTOM_RIGHT_BACK; i <= (int)EQuadrant::TOP_LEFT_FRONT; ++i) {
        this->Children.push_back( std::make_shared<Octree>() );
    }

}

Octree::~Octree()
{
}

// 点插入树中
void Octree::insert(int x, int y, int z)
{
    if (find(x, y, z)) {
        std::cout << "Point already existed." << std::endl;
        return;
    }

    // 点不在范围内
    bool bIsOutOfScope = x < MinRange->x || x > MaxRange->x ||
                         y < MinRange->y || y > MaxRange->y ||
                         z < MinRange->z || z > MaxRange->z;
    if (bIsOutOfScope) {
        std::cout << "Point out of scope." << std::endl;
        return;
    }

    // 3条边界线
    int xMid = (MinRange->x + MaxRange->x) >> 1;
    int yMid = (MinRange->y + MaxRange->y) >> 1;
    int zMid = (MinRange->z + MaxRange->z) >> 1;

    // 象限
    int Quadrant = Util::GetQuadrant(x,y,z,xMid,yMid,zMid);

    // 点的插入

    // 为未分割的方块则进行分割
    if (Children[Quadrant]->RootPoint == nullptr) {
        Children[Quadrant]->insert(x, y, z);
    }
    // 点被方块包含且方块没值，将值存入
    else if (Children[Quadrant]->RootPoint->x == -1) {
        delete Children[Quadrant].get();
        Children[Quadrant] = std::make_shared<Octree>(x, y, z);
    }
    // 点被方块包含且方块有值
    else {
        // 将旧值暂存
        int xOld = Children[Quadrant]->RootPoint->x;
        int yOld = Children[Quadrant]->RootPoint->y;
        int zOld = Children[Quadrant]->RootPoint->z;
        delete Children[Quadrant].get();
        Children[Quadrant] = nullptr;

        EQuadrant QuadrandCastEnum = static_cast<EQuadrant>(Quadrant);
        // 重新创建立方体，进行更细的分割
        if (QuadrandCastEnum == EQuadrant::BOTTOM_RIGHT_BACK) {
            Children[Quadrant] = std::make_shared<Octree>(xMid , MinRange->x, 
                                                          yMid , MinRange->y,
                                                          zMid , MinRange->z);
        }
        else if (QuadrandCastEnum == EQuadrant::BOTTOM_RIGHT_FRONT) {
            Children[Quadrant] = std::make_shared<Octree>(xMid , MinRange->x,
                                                          MaxRange->y , yMid,
                                                          zMid , MinRange->z);
        }
        else if (QuadrandCastEnum == EQuadrant::BOTTOM_LEFT_BACK) {
            Children[Quadrant] = std::make_shared<Octree>(MaxRange->x, xMid,
                                                          yMid, MinRange->y,
                                                          zMid, MinRange->z);
        }
        else if (QuadrandCastEnum == EQuadrant::BOTTOM_LEFT_FRONT) {
            Children[Quadrant] = std::make_shared<Octree>(MaxRange->x, xMid,
                                                          MaxRange->y, yMid,
                                                          zMid, MinRange->z);
        }
        else if (QuadrandCastEnum == EQuadrant::TOP_RIGHT_BACK) {
            Children[Quadrant] = std::make_shared<Octree>(xMid, MinRange->x,
                                                          yMid, MinRange->y,
                                                          MaxRange->z,zMid);
        }
        else if (QuadrandCastEnum == EQuadrant::TOP_RIGHT_FRONT) {
            Children[Quadrant] = std::make_shared<Octree>(xMid, MinRange->x,
                                                          MaxRange->y, yMid,
                                                          MaxRange->z, zMid);
        }
        else if (QuadrandCastEnum == EQuadrant::TOP_LEFT_BACK) {
            Children[Quadrant] = std::make_shared<Octree>(MaxRange->x, xMid,
                                                          yMid, MinRange->y,
                                                          MaxRange->z, zMid);
        }
        else if (QuadrandCastEnum == EQuadrant::TOP_LEFT_FRONT) {
            Children[Quadrant] = std::make_shared<Octree>(MaxRange->x, xMid,
                                                          MaxRange->y, yMid,
                                                          MaxRange->z, zMid);
        }
        // 将原值放入子节点
        Children[Quadrant]->insert(xOld, yOld, zOld);
        Children[Quadrant]->insert(x, y, z);

    }


}

// 查找点
bool Octree::find(int x, int y, int z)
{
    if (RootPoint == nullptr) {
        return false;
    }

    if (RootPoint->x == x && RootPoint->y == y && RootPoint->z == z) {
        std::cout << "Point exist." << std::endl;
        return true;
    }

    // 3条边界线
    int xMid = (MinRange->x + MaxRange->x) >> 1;
    int yMid = (MinRange->y + MaxRange->y) >> 1;
    int zMid = (MinRange->z + MaxRange->z) >> 1;

    // 象限
    int Quadrant = Util::GetQuadrant(x, y, z, xMid, yMid, zMid);

    // 找到值
    if (Children[Quadrant]->RootPoint->x == x && Children[Quadrant]->RootPoint->y && Children[Quadrant]->RootPoint->z) {
        std::cout << "Point find succeed." << std::endl;
        return true;
    }
    // 值不存在继续往跟下层立方体查找
    else if (Children[Quadrant] == nullptr) {
        return Children[Quadrant]->find(x, y, z);
    }

    return false;
}


