#pragma once
#include<iostream>
#include<vector>
#include<memory>

#include "Util.h"
#include "Point.h"
#include "Define.h"

class Octree
{
public:
	Octree();
	Octree(int x, int y, int z);
	Octree(int xMax, int xMin, int yMax, int yMin, int zMax, int zMin);
	~Octree();
	bool find(int x, int y, int z);
	void insert(int x, int y, int z);

private:
	PointPtr RootPoint = nullptr;
	// �߽�
	PointPtr MaxRange = nullptr;
	PointPtr MinRange = nullptr;
	// �˸��ڵ�
	std::vector<OctreePtr> Children;
};

