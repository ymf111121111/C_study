#pragma once
#include<iostream>
#include "Util.h"
#include "Define.h"

int Util::GetQuadrant(int x, int y, int z , int xMid , int yMid , int zMid) {
    EQuadrant Quadrant;
    // TODO �����Ż�
    if (x <= xMid && y <= yMid && z <= zMid) {
        Quadrant = EQuadrant::BOTTOM_RIGHT_BACK;
    }
    else if (x <= xMid && y >= yMid && z <= zMid) {
        Quadrant = EQuadrant::BOTTOM_RIGHT_FRONT;
    }
    else if (x >= xMid && y <= yMid && z <= zMid) {
        Quadrant = EQuadrant::BOTTOM_LEFT_BACK;
    }
    else if (x >= xMid && y >= yMid && z <= zMid) {
        Quadrant = EQuadrant::BOTTOM_LEFT_FRONT;
    }
    else if (x <= xMid && y <= yMid && z >= zMid) {
        Quadrant = EQuadrant::TOP_RIGHT_BACK;
    }
    else if (x <= xMid && y >= yMid && z >= zMid) {
        Quadrant = EQuadrant::TOP_RIGHT_FRONT;
    }
    else if (x >= xMid && y <= yMid && z >= zMid) {
        Quadrant = EQuadrant::TOP_LEFT_BACK;
    }
    else if (x >= xMid && y >= yMid && z >= zMid) {
        Quadrant = EQuadrant::TOP_LEFT_FRONT;
    }

    return static_cast<int>(Quadrant);
}
