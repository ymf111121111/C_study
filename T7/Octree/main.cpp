#pragma once
#include<iostream>

#include "Octree.h"

int main() {
    Octree tree(1, 1, 1, 4, 4, 4);
    std::cout << "Insert (3, 3, 3)\n";
    tree.insert(3, 3, 3);
    std::cout << "Insert (3, 3, 4)\n";
    tree.insert(3, 3, 4);

    std::cout << "Find (3, 3, 3):\n";
    std::cout << (tree.find(3, 3, 3) ? "True\n" : "False\n");
    std::cout << "Find (3, 4, 4):\n";
    std::cout << (tree.find(3, 4, 4) ? "True\n" : "False\n");
    std::cout << "Insert (3, 4, 4)\n";
    tree.insert(3, 4, 4);
    std::cout << "Find (3, 4, 4):\n";
    std::cout << (tree.find(3, 4, 4) ? "True\n" : "False\n");
    return 0;
}