#include "SkipList.h"

// Uitl 

// 1层百分之50 ， 2层百分之25。
int randomLevel()
{
    int level = 1;
    while (rand() / static_cast<double>(RAND_MAX) < PERCENT && level <= LEVEL_MAX) {
        level += 1;
    }
    return level;
}

Node::Node(int val)
{
    data = val;
}



// SkipList
SkipList::SkipList()
{
    head = std::make_shared<Node>();
}

SkipList::~SkipList()
{
}

bool SkipList::Search(int target)
{
    return false;
}

void SkipList::Add(int val)
{
    NodePtr newNode = std::make_shared<Node>(val);
    int level = randomLevel();
    newNode->currLevel = level;

    Node* tempHead = head.get();
    for (int i = level - 1; i >= 0; --i) {
        while (tempHead->nextList[i] != nullptr && tempHead->nextList[i]->data > val) {
            newNode->nextList[i] = tempHead->nextList[i];
        }
        head->nextList[i] = newNode;
    }
}


bool SkipList::Erase(int num)
{
    
    return false;
}

