#pragma once
#include<iostream>
#include<vector>
#include<math.h>

#define LEVEL_MAX 4
#define PERCENT 0.5
struct Node;
using NodePtr = std::shared_ptr<Node>;

const int N = 10;
struct Node
{
    int data;
    int currLevel = 1;
    std::vector<NodePtr> nextList { std::vector<NodePtr>(LEVEL_MAX,nullptr)};
    Node(int Val);
};

class SkipList
{
private:
    NodePtr head = nullptr;

public:
    SkipList();
    ~SkipList();
    // ��
    void Add(int num);
    // ɾ
    bool Erase(int num);
    // ��
	bool Search(int target);

};

